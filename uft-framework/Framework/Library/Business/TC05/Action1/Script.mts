﻿

iRowCount = Datatable.getSheet("TC05 [TC05]").getRowCount
username = Trim((DataTable("Username","TC05 [TC05]")))
password = Trim((DataTable("Password","TC05 [TC05]")))



Call FW_TransactionStart("TC05 OpenWebShopping")
Call FW_OpenWebBrowser("https://demostore.x-cart.com/","CHROME")
Call FW_TransactionEnd("TC05 OpenWebShopping")


'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebButton("{'url_params':{'target':'login").Click @@ script infofile_;_ZIP::ssf1.xml_;_
Call FW_TransactionStart("TC05 ClickLoginButton")
Call FW_WebElement("TC05","X-Cart Demo store company","X-Cart Demo store company","Sign in / sign up")
Call FW_TransactionEnd("TC05 ClickLoginButton")
 @@ script infofile_;_ZIP::ssf2.xml_;_


Call FW_TransactionStart("TC05 InputUsername")
Call FW_WebEdit("TC05","X-Cart Demo store company","X-Cart Demo store company","login", username)
Call FW_TransactionEnd("TC05 InputUsername")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebEdit("login").Set

Call FW_TransactionStart("TC05 InputPassword")
Call FW_WebEdit("TC05","X-Cart Demo store company","X-Cart Demo store company","password", password)
Call FW_TransactionEnd("TC05 InputPassword")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebEdit("password").SetSecure "5ec4ad1e2a3131c39062e8fc01a0cce4c6084d7f58cf7ffc31691e3ca1c586bc" @@ script infofile_;_ZIP::ssf3.xml_;_

Call FW_TransactionStart("TC05 ClickSign in")
Call FW_WebButton("TC05","X-Cart Demo store company","X-Cart Demo store company","Sign in")
Call FW_TransactionEnd("TC05 ClickSign in")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebButton("Sign in").Click @@ script infofile_;_ZIP::ssf4.xml_;_

Call FW_TransactionStart("TC05 ClickShippingManu")
Call FW_Link("TC05","X-Cart Demo store company","X-Cart Demo store company","Shipping")
Call FW_TransactionEnd("TC05 ClickShippingManu")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").Link("Shipping").Click @@ script infofile_;_ZIP::ssf5.xml_;_

Call FW_TransactionStart("TC05 ClickElectronics")
Call FW_Link("TC05","X-Cart Demo store company","X-Cart Demo store company","Electronics")
Call FW_TransactionEnd("TC05 ClickElectronics")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").Link("Electronics").Click @@ script infofile_;_ZIP::ssf6.xml_;_

Call FW_TransactionStart("TC05 ClickSmart watches")
Call FW_WebElement("TC05","X-Cart Demo store company","X-Cart Demo store company","Smart watches")
Call FW_TransactionEnd("TC05 ClickSmart watches")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").Link("Smart watches").Click @@ script infofile_;_ZIP::ssf7.xml_;_

Call FW_TransactionStart("TC05 Select Bluetooth Smartwatch with")
Call FW_Link("TC05","X-Cart Demo store company","X-Cart Demo store company","Bluetooth Smartwatch with")
Call FW_TransactionEnd("TC05 Select Bluetooth Smartwatch with")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").Link("Bluetooth Smartwatch with").Click @@ script infofile_;_ZIP::ssf8.xml_;_

Call FW_TransactionStart("TC05 Click Add to cart")
Call FW_WebButton("TC05","X-Cart Demo store company","X-Cart Demo store company","Add to cart")
Call FW_TransactionEnd("TC05 Click Add to cart")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebButton("Add to cart").Click @@ script infofile_;_ZIP::ssf9.xml_;_

Call FW_TransactionStart("TC05 Click Checkout")
Call FW_Link("","X-Cart Demo store company","X-Cart Demo store company","Checkout")
Call FW_TransactionEnd("TC05 Click Checkout")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").Link("Checkout").Click
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebRadioGroup("methodId").Select "#1" @@ script infofile_;_ZIP::ssf11.xml_;_

Call FW_TransactionStart("TC05 Click Proceed to payment")
Call FW_WebButton("TC05","X-Cart Demo store company","X-Cart Demo store company","Proceed to payment")
Call FW_TransactionEnd("TC05 Click Proceed to payment")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebButton("Proceed to payment").Click @@ script infofile_;_ZIP::ssf12.xml_;_

Call FW_TransactionStart("TC05 Select COD Cash On Delivery")
Call FW_WebRadioGroup("TC05","X-Cart Demo store company","X-Cart Demo store company","methodId","#1")
'FW_WebRadioGroup(actionName, objBrowser, objPage, objRadioGroup, Index)
'Call FW_WebElement("TC05","X-Cart Demo store company_3","X-Cart Demo store company","COD")
Call FW_TransactionEnd("TC05 Select COD Cash On Delivery")
'
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebRadioGroup("methodId").Select "#1" @@ script infofile_;_ZIP::ssf13.xml_;_

Call FW_TransactionStart("TC05 Click Place order")
Call FW_WebButton("TC05","X-Cart Demo store company","X-Cart Demo store company","Place order")
Call FW_TransactionEnd("TC05 Click Place order")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebButton("Place order").Click @@ script infofile_;_ZIP::ssf14.xml_;_

Call FW_TransactionStart("TC05 CheckMessage")
Call FW_CheckMessage("TC05","X-Cart Demo store company","X-Cart Demo store company","page-title","Thank you for your order")
'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company_5").WebElement("page-title").Click
Call FW_TransactionEnd("TC05 CheckMessage")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebElement("page-title").Click @@ script infofile_;_ZIP::ssf15.xml_;_

Call FW_TransactionStart("TC05 Click My account")
Call FW_WebElement("TC05","X-Cart Demo store company","X-Cart Demo store company","My account")
Call FW_TransactionEnd("TC05 Click My account")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebElement("My account").Click @@ script infofile_;_ZIP::ssf16.xml_;_

Call FW_TransactionStart("TC05 Click Log out")
Call FW_Link("TC05","X-Cart Demo store company","X-Cart Demo store company","Log out")
Call FW_TransactionEnd("TC05 Click Log out")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").Link("Log out").Click @@ script infofile_;_ZIP::ssf17.xml_;_


FW_CloseWebBrowser("CHROME")
